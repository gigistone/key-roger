#!/bin/sh
### BEGIN INIT INFO
# Provides: ROGER-FIREWALL
# Required-Start:
# Required-Stop:
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 5
# Description: Roger skyline strict firewall
### END INIT INFO

# reset all to default
iptables -F
iptables -X

# policy default DROP
iptables -P INPUT   DROP
iptables -P OUTPUT  DROP
iptables -P FORWARD DROP

# accept all already established connections
iptables -A INPUT  -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

########################################
### OUVERTURES DES PORTS NECESSAIRES ###
########################################

# SSH
iptables -A INPUT  -p tcp --dport 7777 -j ACCEPT

# DNS
iptables -A OUTPUT -p tcp --dport 53 -j ACCEPT
iptables -A OUTPUT -p udp --dport 53 -j ACCEPT

# http/s client
iptables -A OUTPUT -p tcp --dport 80  -j ACCEPT
iptables -A OUTPUT -p tcp --dport 443 -j ACCEPT

# http/s server
iptables -A INPUT -p tcp --dport 80  -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT


############################################
### PROTECTION DES PORTS CONTRE LES DDOS ###
############################################
# gestion des spoof ip
# protection contre les flood a base de tcp
# protection contre les flood icmp
# protection contre les SYN floods
#
# toutes ces protections sont mises en place
# en preroutage, le but etant de les mitiger
# avant qu elles ne passent l entree de la NIC
#
# !! TOUTES CES PROTECTIONS SONT IMPUISSANTES
# contre des attaques massives combinees !!
#############################################

# blocage de tous les paquets en etat invalide
# et des paquets new qui ne sont pas des SYN
iptables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP
iptables -t mangle -A PREROUTING -p tcp ! --syn -m conntrack --ctstate NEW -j DROP

# blocage des nouveaux syn qui ont une taille etrange
iptables -t mangle -A PREROUTING -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP

#blocage des paquets paquets inutilisables (dus a leur flags)
iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN FIN,SYN -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags SYN,RST SYN,RST -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,RST FIN,RST -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,ACK FIN -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,URG URG -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,FIN FIN -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,PSH PSH -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL ALL -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL NONE -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL FIN,PSH,URG -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,FIN,PSH,URG -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP

# blocage de type private pour contrer le spoof d ip,
#iptables -t mangle -A PREROUTING -s 224.0.0.0/3 -j DROP
#iptables -t mangle -A PREROUTING -s 169.254.0.0/16 -j DROP 
#iptables -t mangle -A PREROUTING -s 172.16.0.0/12 -j DROP 
#iptables -t mangle -A PREROUTING -s 192.0.2.0/24 -j DROP 
#iptables -t mangle -A PREROUTING -s 192.168.0.0/16 -j DROP 
#iptables -t mangle -A PREROUTING -s 10.0.0.0/8 -j DROP 
#iptables -t mangle -A PREROUTING -s 0.0.0.0/8 -j DROP 
#iptables -t mangle -A PREROUTING -s 240.0.0.0/5 -j DROP 
#iptables -t mangle -A PREROUTING -s 127.0.0.0/8 ! -i lo -j DROP

# blocage de tous les paquets icmp (echo compris)
iptables -t mangle -A PREROUTING -p icmp -j DROP

# blocage des SYN attack avec synproxy, au cas ou elles seraient passees
# au travers des regles precedentes. 

iptables -t raw -A PREROUTING -p tcp -m tcp --syn --dport 443 -j CT --notrack 
iptables -A INPUT -p tcp -m tcp --dport 80 -m conntrack --ctstate INVALID,UNTRACKED -j SYNPROXY --sack-perm --timestamp --wscale 7 --mss 1460 
iptables -A INPUT -m conntrack --ctstate INVALID -j DROP
iptables -t raw -A PREROUTING -p tcp -m tcp --syn --dport 443 -j CT --notrack 
iptables -A INPUT -p tcp -m tcp --dport 443 -m conntrack --ctstate INVALID,UNTRACKED -j SYNPROXY --sack-perm --timestamp --wscale 7 --mss 1460 
iptables -A INPUT -m conntrack --ctstate INVALID -j DROP
