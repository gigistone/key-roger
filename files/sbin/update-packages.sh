#!/bin/bash

LOG_FILE="/var/log/update_script.log"

echo "[$(date)][UPDATE]" >> $LOG_FILE
apt-get update >> $LOG_FILE
echo "[$(date)][UPGRADE]" >> $LOG_FILE
apt-get upgrade -y >> $LOG_FILE
