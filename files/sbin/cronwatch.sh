#!/bin/bash

TARGET="/etc/crontab"
SCRIPT_DIR=$(pwd)
CRONWATCH_DIR="$SCRIPT_DIR/cronwatch.d"
CRON_SUM="cron_shasum"

CRON_SUM_PATH="$CRONWATCH_DIR/$CRON_SUM"

mkdir -p $CRONWATCH_DIR

if ! [ -f "$CRON_SUM_PATH" ];then
  shasum -a 256 $TARGET > $CRON_SUM_PATH
  exit
fi

if ! shasum -a 256 -c $CRON_SUM_PATH &> /dev/null ;then
  echo "[$(date)] crontab was modified" | mail -s "[IMPORTANT] CRONWATCH" root
fi
